// GridMatch.cpp : Defines the entry point for the console application.

#define USE_GPU 

#include "Header.h"
#include "gms_matcher.h"

int GmsMatch_image(Mat &img1, Mat &img2);
// GmsMatch_match_num(Mat &img1, Mat &img2);

double Sizeofimg(double length,double width);
int roi_size = 800;

void runImagePair() {

	string target_item;
	string target_item_suffix;
	cout << "Please input target name" << endl;
	cin >> target_item;
	Mat img_roi = imread("1.jpg", CV_LOAD_IMAGE_ANYCOLOR);
	//Mat img_item = imread("3.png", CV_LOAD_IMAGE_ANYCOLOR);
	
	imresize(img_roi, roi_size);
	
	
	/////////////find a rough position of target item/////////////////////
	int x = 0, y = 0;
	int a = 0, b = 0;
	int max_match = 0;
	double length, width;
	double size;
	cout << "Please input length and width (unit is meter)" << endl;
	cin >> length >> width;
	size = Sizeofimg(length,width);
	for (int i = 1; i <= 6; i++)
	{
		switch (i)
		{
		case 1:
			target_item_suffix = "_Top_01.png"; break;
		case 2:
			target_item_suffix = "_Bottom_01.png"; break;
		case 3:
			target_item_suffix = "_Top-Side_01.png"; break;
		case 4:
			target_item_suffix = "_Top-Side_02.png"; break;
		case 5:
			target_item_suffix = "_Bottom-Side_01.png"; break;
		case 6:
			target_item_suffix = "_Bottom-Side_02.png"; break;
		}
		Mat img_item = imread("/dataset/arc_datasets/" + target_item + "/" + target_item + target_item_suffix, 1);
		if (!img_item.data)
		{
			cout << "none such file " << target_item << endl;
			//system("pause");
			continue;
		}
		imresize(img_item, size);
		if (length > 0.19)
		{
			for (int x = 0; x <= img_roi.cols / 2; x = x + img_roi.cols / 4)
			{
				for (int y = 0; y <= img_roi.rows / 2; y = y + img_roi.rows / 4)
				{
					Mat sub_roi = img_roi(Rect(x, y, img_roi.cols / 2, img_roi.rows / 2));
					int match_num = GmsMatch_image(sub_roi, img_item);
					if (max_match < match_num)
					{
						max_match = match_num;
						a = x; b = y;
					}
				}
			}
			cout << a << "   " << b << endl;
			Mat result_img = img_roi(Rect(a, b, img_roi.cols / 2, img_roi.rows / 2));
			GmsMatch_image(img_roi, img_item);
			imshow("Result", result_img);
			waitKey();
		}
		else
		{
			double lengthofimg = roi_size / 0.55*length / 1.414 + roi_size / 0.55*width / 1.414;
			int cols_times = img_roi.cols / lengthofimg + 1;
			int rows_times = img_roi.rows / lengthofimg + 1;
			int overlap_cols = (cols_times*lengthofimg - img_roi.cols) / (cols_times - 1);
			int overlap_rows = (rows_times*lengthofimg - img_roi.rows) / (rows_times - 1);
			for (int x = 0; x <= img_roi.cols - lengthofimg; x = x + (img_roi.cols-lengthofimg)/2/(cols_times-1))
			{
				for (int y = 0; y <= img_roi.rows - lengthofimg; y = y + (img_roi.rows - lengthofimg)/2/(rows_times - 1))
				{
					//cout << x <<"  "<< y;
					Mat sub_roi = img_roi(Rect(x, y, lengthofimg, lengthofimg));
					int match_num = GmsMatch_image(sub_roi, img_item);
					if (max_match < match_num)
					{
						max_match = match_num;
						a = x; b = y;
					}
				}
			}
			cout << a << "   " << b << endl;
			Mat result_img = img_roi(Rect(a, b, lengthofimg, lengthofimg));
			GmsMatch_image(img_roi, img_item);
			imshow("Result", result_img);
			waitKey();
		}
	}

	//////////////find rough position of target item//////////


	//////////////find suction pose//////
	/*
	int p_a, p_b;
	int sub_max_match = 0;
	for (int x1 = 0; x1 <= result_img.cols *3/4; x1 = x1 + result_img.cols / 8)
	{
		for (int y1 = 0; y1 <= result_img.rows *3/4; y1 = y1 + result_img.rows / 8)
		{
			Mat ssub_roi = result_img(Rect(x1, y1, result_img.cols / 4, result_img.rows / 4));
			
			////////////sub target image/////////////////
			for (int x2 = 0; x2 <= img_item.cols * 3/4; x2 = x2 + img_item.cols / 8)
			{
				for (int y2 = 0; y2 <= img_item.rows *3/4; y2 = y2 + img_item.rows / 8)
				{
					Mat ssub_item = img_item(Rect(x2, y2, img_item.cols / 4, img_item.rows / 4));
					int s_match_num = GmsMatch_image(ssub_roi, ssub_item);
					if (sub_max_match < s_match_num)
					{
						sub_max_match = s_match_num;
						p_a = x1; p_b = y1;
					}
				}
			}

			//////////////////////////////////////
			
		}
	}
	cout << p_a << "   " << p_b << endl;
	Mat sub_result_img = result_img(Rect(p_a, p_b, result_img.cols / 4, result_img.rows / 4));
	GmsMatch_image(sub_result_img, img_item);
	imshow("sub_Result", sub_result_img);
	waitKey();
	*/
	////////////////find sucktion pose//////////
	//system("pause");
	

	//GmsMatch_image(img1, img2);
	//cout << GmsMatch_image(img1, img2) << endl;
	//GmsMatch_match_num(img1, img2);
	//system("pause");

}
double Sizeofimg(double length, double width)
{
	//cout << "Please input the max dimension of the target item (unit is METER)" << endl;
	double dimension=max(length,width);
	//cin >> dimension;
	double size = roi_size / 0.5*dimension;
	return size;
}

int main()
{
#ifdef USE_GPU
	int flag = cuda::getCudaEnabledDeviceCount();
	if (flag != 0) { cuda::setDevice(0); }
#endif // USE_GPU

	runImagePair();

	return 0;
}


int GmsMatch_image(Mat &img1, Mat &img2) {
	vector<KeyPoint> kp1, kp2;
	Mat d1, d2;
	vector<DMatch> matches_all, matches_gms;

	Ptr<ORB> orb = ORB::create(10000);
	orb->setFastThreshold(0);
	orb->detectAndCompute(img1, Mat(), kp1, d1);
	orb->detectAndCompute(img2, Mat(), kp2, d2);

#ifdef USE_GPU
	GpuMat gd1(d1), gd2(d2);
	Ptr<cuda::DescriptorMatcher> matcher = cv::cuda::DescriptorMatcher::createBFMatcher(NORM_HAMMING);
	matcher->match(gd1, gd2, matches_all);
#else
	BFMatcher matcher(NORM_HAMMING);
	matcher.match(d1, d2, matches_all);
#endif

	// GMS filter
	int num_inliers = 0;
	std::vector<bool> vbInliers;
	gms_matcher gms(kp1, img1.size(), kp2, img2.size(), matches_all);
	num_inliers = gms.GetInlierMask(vbInliers, false, true);

	cout << "Get total " << num_inliers << " matches." << endl;

	// draw matches
	for (size_t i = 0; i < vbInliers.size(); ++i)
	{
		if (vbInliers[i] == true)
		{
			matches_gms.push_back(matches_all[i]);
		}
	}
	
	//Mat show = DrawInlier(img1, img2, kp1, kp2, matches_gms, 1);
	//imshow("show", show);
	//waitKey();
	return num_inliers;
	
}
